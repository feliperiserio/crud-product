package com.avenueCode.test;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.derby.drda.NetworkServerControl;

import com.avenueCode.model.Product;
import com.avenueCode.util.JPAUtil;
import com.google.gson.Gson;

public class TestDatabase {

	public static void main(String[] args) throws Exception
	  {
		/*new JHades().overlappingJarsReport(); */
		
		NetworkServerControl server = new NetworkServerControl();
		server.start (null);
		
		// create EntityManager
		EntityManager em = new JPAUtil().getEntityManager();

		Product p = new Product();
		p.setName(Math.random() + "");
		p.setDescription("aaaaaaa22222222");
		
		// Persist
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		
		System.out.println(new Gson().toJson(p));
		
		List<Product> list = em.createQuery("FROM Product", Product.class).getResultList(); 
		
		for (Product object : list) {
			System.out.println(">>>> ID: "+object.getId()+" "+" Name:"+object.getName());
		}
		// close EntityManager and EntityManagerFactory
		em.close();
		
	  }
}
