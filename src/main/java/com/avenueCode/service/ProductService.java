package com.avenueCode.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.avenueCode.model.Product;
import com.avenueCode.util.JPAUtil;
import com.google.gson.Gson;

@Path("/product")
public class ProductService {
	EntityManager em = new JPAUtil().getEntityManager();
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public String listProduct(){
		try {
			List<Product> productList = em.createQuery("FROM Product", Product.class).getResultList(); 
			return new Gson().toJson(productList);
		} catch (Exception e) {
			throw new WebApplicationException(500); 
		}
	}
	
	@GET
	@Path("/search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String searchProduct(@PathParam("id") int id){
		try {
			EntityManager em = new JPAUtil().getEntityManager();
			Product p = em.find(Product.class, id);
			return new Gson().toJson(p);
		} catch (Exception e) {
			throw new WebApplicationException(500); 
		}
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(Product p){
		try{
			// create EntityManager
			EntityManager em = new JPAUtil().getEntityManager();
			
			em.getTransaction().begin();
			em.merge(p);
			em.getTransaction().commit();
			em.close();
			
			return Response.status(200).entity("success").build();
		}catch (Exception e){
			throw new WebApplicationException(500);
		}
	}
	
	 @DELETE
	 @Path("/delete/{id}")
	 public Response delete(@PathParam("id") int id) {
		 try {
			 if (id == 0) 
				 return Response.status(500).entity("You must set the field id.").build();
		  	 
			// create EntityManager
			EntityManager em = new JPAUtil().getEntityManager();

			Product p = em.find(Product.class, id);
			
			em.getTransaction().begin();
			em.remove(p);
			em.getTransaction().commit();
			em.close();
			
			return Response.status(200).entity("Product removed with success!").build();
		} catch (Exception e) {
			throw new WebApplicationException(500); 
		}
	 }
	
	 @PUT
	 @Path("/insert")
	 @Consumes(MediaType.APPLICATION_JSON)
	 public Response insert(Product p) {
		 try {
			 if (p.getId() != 0) 
				 return Response.status(500).entity("You can't set the field id.").build();
		  	 
			// create EntityManager
			EntityManager em = new JPAUtil().getEntityManager();

			// Persist
			em.getTransaction().begin();
			em.persist(p);			
			em.getTransaction().commit();
			
			String result = "Success!";
			return Response.status(201).entity(result).build();
		} catch (Exception e) {
			throw new WebApplicationException(500); 
		}
	 }
}
